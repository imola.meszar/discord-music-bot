# Discord Music Bot
Functionalities:
1. Youtube radio
    * Play
    * Shuffle
    * Clear playlist
    * List playlist
    * Play youtube playlist/watchlist (including youtube generated mix which does not have playlist keyword in URL)
2. Online radio
    * Play
    * List available radio stations
3. Common
    * Next
    * Stop

Good to know:
* The common part has the playlist functionality so take care not to shuffle the radio stations :)

# TODO items:
1. Thanks to the main event loop, adding a watchlist link now blocks (no idea yet how to fix), so the watchlist has to be parsed till the end and only after that will the songs start playing. Somebody should fix this, pretty please :)
