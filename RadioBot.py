from discord import FFmpegPCMAudio
from discord.ext import commands
import asyncio

class RadioBot(commands.Cog):
  radio_stations = {
    "danu": "https://www.danubiusradio.hu/live_320.mp3/",
    "danub": "https://www.danubiusradio.hu/live_320.mp3/",

    "juv": "https://s2.audiostream.hu/juventus_320k",
    "juventus": "https://s2.audiostream.hu/juventus_320k",

    "magic": "https://live.magicfm.ro:8443/magicfm.aacp",
    "paprika": "http://stream1.paprikaradio.ro:8000",
    "petofi": "https://icast.connectmedia.hu/4738/mr2.mp3",
    "radio1": "https://icast.connectmedia.hu/5202/live.mp3",
    "retro": "https://icast.connectmedia.hu/5002/live.mp3",
    "rock": "https://s2.audiostream.hu/bdpstrock_192k",
    "slager": "http://92.61.114.159:7812/slagerfm256.mp3",
  }

  def __init__(self, bot):
    self.bot = bot

  @commands.command(
    name = "rlist",
    description = "List the available radios",
    pass_context = True,
    help = "List the available radios"
  )
  async def rlist(self, context):
    self.log("rlist")
    i = 0
    msg = ""
    for radio_station in self.radio_stations:
      msg += f"radio[{i}]: {radio_station} - {self.radio_stations[radio_station]}\n"
      i += 1
    await context.channel.send(msg)

  @commands.command(
    name = "rplay",
    description = "Start a radio station",
    pass_context = True,
    help = "Start a radio station. Use -rlist to get available radios"
  )
  async def rplay(self, context, *args):
    self.log("rplay")

    try:
      channel_name = context.message.author.voice.channel.name
    except:
      await context.channel.send(f"Dear {context.message.author}, you are not connected to any voice channel. Can't start any radio...")
      return

    radio_station_selected = " ".join(args)
    radio_station_found = False
    for radio_station in self.radio_stations:
      if radio_station_selected == radio_station:
        radio_station_found = True
        break
    if not radio_station_found:
      await context.channel.send(f"{radio_station_selected} could not be found. Did you mistype it? Use -rlist to check the radio stations")

    await context.channel.send(f"{radio_station_selected} added to playlist. Use -next if needed")
    radio_station_url = self.radio_stations[radio_station_selected]
    source = source = FFmpegPCMAudio(radio_station_url)
    self.bot.get_cog("CommonBot").AddNextTrack(
      context,
      source,
      "SlagerFM",
      f"Will try to play a radio: {radio_station_selected}")

  def log(self, str):
    print(f"RadioBot: {str}")