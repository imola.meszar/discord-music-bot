import datetime
from discord import opus, Intents
from discord.ext import commands
import logging

from YoutubeBot import YoutubeBot
from CommonBot import CommonBot
from RadioBot import RadioBot

from VoiceClientFix import apply_fix

intents = Intents.all()

bot = commands.Bot(command_prefix='-', intents=intents)

@bot.event
async def setup_hook():
  # Add Cogs to Bot
  print("Adding cogs")  
  await bot.add_cog(CommonBot(bot))
  await bot.add_cog(YoutubeBot(bot))
  await bot.add_cog(RadioBot(bot))
  print("Cogs added")

@bot.event
async def on_ready():
  print("on_ready")
  print(datetime.datetime.now())
  # Tell the users from LemonTree server that the Bot is up and running
  try:
    print("Sending `ready` message to LemonTree server")
    zenemalacz_channel = bot.get_channel(784022447183757323)
    await zenemalacz_channel.send("LemonTreeBot up and running :white_check_mark:")
  except Exception as e:
    print(f"Could not send `ready` message to LemonTree server ${e}")
    pass


def main():
  # Apply Voice Client fix: https://github.com/Rapptz/discord.py/issues/9277
  apply_fix()
  # Load needed libs on linux
  # LoadOpusLib()
  # Bok Token
  token = "OTQyODM2NTUxMzU0ODQzMTg2.G5lTXk.f7WUGLUnfLCZqcfo2QgLkkkptHzYuTNFZA6R24"  
  # Logger
  handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
  # Start the bot
  bot.add_command(connect)
  bot.run(token, log_handler=handler, log_level=logging.DEBUG)

@commands.command()
async def connect(context):
    print("connect")
    user = context.message.author
    voice_channel = None
    voice_client = None
    try:
      voice_channel = user.voice.channel
    except:
      print("Could not get voice_channel")
    if voice_channel != None:
      if voice_client is None:
        print("voice_client is None")
        print(type(voice_channel))
        voice_client = await voice_channel.connect()
        print(f"voice_client is {voice_channel}")

def LoadOpusLib():
  OPUS_LIBS = ['libopus-0.x86.dll', 'libopus-0.x64.dll', 'libopus-0.dll', 'libopus.so.0', 'libopus.0.dylib']

  if opus.is_loaded():
    return True

  for opus_lib in OPUS_LIBS:
    try:
      opus.load_opus(opus_lib)
      print(datetime.datetime.now())
      print("Opus loaded")
      return
    except OSError:
      pass

if __name__ == "__main__":
  main()